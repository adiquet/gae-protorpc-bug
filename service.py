import sys
import os.path
# Add `lib` subdirectory to `sys.path`, so our module can load third-party libraries
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'lib'))

# If protorpc is in ./lib ( $ pip install -r requirements.txt --target ./lib )
# This import will fail because ./lib/protorpc clashes with the GAE protorpc
# This can be seen for example by browsing to the API Explorer at
# https://apis-explorer.appspot.com/apis-explorer/?base=http%3A%2F%2Flocalhost%3A8080%2F_ah%2Fapi#p/
import endpoints
from protorpc import message_types
from protorpc import remote


@endpoints.api(name='testapi',
               version='v1',
               allowed_client_ids=[endpoints.API_EXPLORER_CLIENT_ID],
               scopes=[endpoints.EMAIL_SCOPE])
class UserApiService(remote.Service):
    @endpoints.method(message_types.VoidMessage,
                      message_types.VoidMessage,
                      path='test',
                      http_method='GET',
                      name='test.test')
    def test(self, _):
        return message_types.VoidMessage()


ENDPOINT_APP = endpoints.api_server([UserApiService])
